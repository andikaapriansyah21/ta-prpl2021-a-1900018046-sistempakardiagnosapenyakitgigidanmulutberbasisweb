<?php include "conf/inc.koneksi.php"; ?>
      <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Selamat Datang Di Sistem Pakar Diagnosa Penyakit Gigi dan Mulut</h3>
            </div>
              <div class="panel-body">
                <p class="text-justify">
                Sistem Pakar Diagnosis Gangguan Kepribadian ini menggunakan rule-based reasoning (penalaran berbasis aturan) yaitu 
                penalaran yang menggunakan urutan tertentu untuk mendapatkan kesimpulan akhir. Sehingga untuk mendapatkan suatu kesimpulan,
                diperlukan suatu metode pengecekan tertentu untuk menelusuri sederetan aturan atau kaidah yang telah ada.
                </p>
                <p class="text-justify">
                Penyakit gigi dan mulut merupakan salah satu masalah kesehatan yang banyak
                dikeluhkan oleh masyarakat, karena mengganggu aktivitas pekerjaan setiap 
                hari. Penyakit ini dapat menyerang mulai dari anak-anak sampai dewasa.
                Minimnya pengetahuan kesehatan gigi dan mulut, terbatasnya sumber informasi 
                serta biaya konsultasi yang mahaljuga menyebabkan rendahnya kesabaran 
                masyarakat terhadap kesehatan gigi dan mulut.
                </p>
                <p class="text-center"><a class="btn btn-warning btn-md" href="?page=consultation" role="button">Start Consultation</a></p>
              </div>
      </div>


      <div class="list-group">
        <?php $sql="select * from artikel order by id desc LIMIT 4";
        $rs=mysql_query($sql);
        while($row=mysql_fetch_array($rs)){ ?>
          <a href="?page=read&id=<?php echo $row[0]; ?>" class="list-group-item">
          <h4 class="list-group-item-heading"><?php echo $row['judul']; ?></h4>
          <img style="float:left;margin-right:20px;"src="news/<?php echo $row['foto']; ?>" class="image-rounded" width="170" height="120"/>
          <p class="list-group-item-text-justify">
          <?php echo substr($row['isi'],0,500); ?>
          </p>
          </a>
          <?php } ?>
      </div>