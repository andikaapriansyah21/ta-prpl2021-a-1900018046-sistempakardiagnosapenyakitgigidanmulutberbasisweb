<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Web Dika</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="favicon.ico" />
  </head>
<body>

    <div class="navbar navbar-inverse navbar-static-top" role="navigation">
       <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          </button>
          <a class="navbar-brand" href="?page=welcome">Web Dika</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="?page=welcome">Home</a></li>
            <li><a href="?page=profil">Profil</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Informasi <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="?page=consultation">Konsultasi</a></li>
                <li><a href="?page=article">Artikel Kesehatan</a></li>
              </ul>
            </li>
            <li><a href="?page=guest">Buku Tamu</a></li>
            <li><a href="?page=contact">Kontak Kami</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
